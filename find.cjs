function find(elements, cb, ele){
    for(let ind = 0; ind < elements.length; ind ++){
        if(cb(elements, ind, ele)){
            return elements[ind];
        }
    }
}
module.exports = find;