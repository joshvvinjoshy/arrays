function filter(elements, cb){
    let ans = [];
    for(let ind = 0; ind < elements.length; ind ++){
        if(cb(elements[ind],ind, elements)==true){
            ans.push(elements[ind]);
        }
    }
    return ans;
}
module.exports = filter;