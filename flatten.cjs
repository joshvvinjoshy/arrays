let ans = [];
function flatten(elements,depth=1){
    // console.log(elements);
    if(elements.length == 0){
        return [];
    }
    for(let ind = 0; ind < elements.length; ind++){
        // console.log(elements[ind]);
        // console.log(typeof(elements[ind]));
        if(elements[ind] == undefined){
            continue;
        }
        if(typeof(elements[ind]) == "number" || depth == 0){
            // console.log(elements[ind]);
            ans.push(elements[ind]);
        }
        else{
            flatten(elements[ind],depth-1);
        }
        // console.log(ans);
    }
    // console.log(ans);
    return ans;
}
module.exports = flatten;