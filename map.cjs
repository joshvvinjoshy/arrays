function map(elements, cb){
    let result = [];
    for(let ind = 0; ind < elements.length; ind ++){
        result.push(cb(elements[ind], ind, elements));
    }
    return result;
}
module.exports = map;