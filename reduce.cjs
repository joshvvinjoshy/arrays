function reduce(elements, cb, startingValue){   
    // let ans = 0;
    let ind = 0;

    if(startingValue == undefined){
        startingValue = elements[0];
        ind++;
    }
    for(; ind < elements.length; ind ++){
        startingValue = cb(startingValue, elements[ind], ind, elements);
        // console.log(ans);
    }
    return startingValue;
}
module.exports = reduce;